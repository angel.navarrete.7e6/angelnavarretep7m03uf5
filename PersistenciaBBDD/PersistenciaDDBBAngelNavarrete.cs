﻿using System;
using System.Collections.Generic;
using System.IO;
using Npgsql;

namespace PersistenciaBBDD
{
    public class Pokemon
    {
        public string Pokedex_number { get; set; }
        public string Name { get; set; }
        public string Generation { get; set; }
        public string Type1 { get; set; }
        public string Type2 { get; set; }
        public string Is_legendary { get; set; }

        public Pokemon(string pokedex_number, string name, string generation, string type1, string type2,string is_legendary)
        {
            this.Pokedex_number = pokedex_number;
            this.Name = name;
            this.Generation = generation;
            this.Type1 = type1;
            this.Type2 = type2;
            this.Is_legendary = is_legendary;
        }
        public Pokemon(string pokedex_number, string name, string generation, string type1, string is_legendary)
        {
            this.Pokedex_number = pokedex_number;
            this.Name = name;
            this.Generation = generation;
            this.Type1 = type1;
            this.Is_legendary = is_legendary;
        }
        public override string ToString()
        {
            if (this.Type2 != null)
            {
                return "Pokemon " + this.Pokedex_number + ": " + this.Name + ", " + this.Generation + " GEN, Type1:" + this.Type1 + ", Type2: " + this.Type2+", Legendary: "+this.Is_legendary;
            }
            else
            {
                return "Pokemon " + this.Pokedex_number + ": " + this.Name + ", " + this.Generation + " GEN, Type1:" + this.Type1 +", Legendary: " + this.Is_legendary;
            }
        }
    }

    class PersistenciaDDBBAngelNavarrete
    {
        private static void CreateTable(NpgsqlConnection con)
        {
            string sql = "CREATE TABLE pokemon (" +
                  "abilities CHAR (250)  ," +
                  "against_bug CHAR (250)," +
                  "against_dark CHAR (250)," +
                  "against_dragon CHAR (250)," +
                  "against_electric CHAR (250)," +
                  "against_fairy CHAR (250)," +
                  "against_fight CHAR (250)," +
                  "against_fire CHAR (250)," +
                  "against_flying CHAR (250)," +
                  "against_ghost CHAR (250)," +
                  "against_grass CHAR (250)," +
                  "against_ground CHAR (250)," +
                  "against_ice CHAR (250)," +
                  "against_normal CHAR (250)," +
                  "against_poison CHAR (250)," +
                  "against_psychic CHAR (250)," +
                  "against_rock CHAR (250)," +
                  "against_steel CHAR (250)," +
                  "against_water CHAR (250)," +
                  "attack CHAR (250)," +
                  "base_egg_steps CHAR (250)," +
                  "base_happiness CHAR (250)," +
                  "base_total CHAR (250)," +
                  "capture_rate CHAR (250)," +
                  "classfication CHAR (250)," +
                  "defense CHAR (250)," +
                  "experience_growth CHAR (250)," +
                  "height_m CHAR (250)," +
                  "hp CHAR (250)," +
                  "name CHAR (250)," +
                  "percentage_male CHAR (250)," +
                  "pokedex_number CHAR (250)," +
                  "sp_attack CHAR (250)," +
                  "sp_defense CHAR (250)," +
                  "speed CHAR (250)," +
                  "type1 CHAR (250)," +
                  "type2 CHAR (250)," +
                  "weight_kg CHAR (250)," +
                  "generation CHAR (250)," +
                  "is_legendary CHAR (250))";

            NpgsqlCommand commmand = new NpgsqlCommand(sql, con);
            commmand.ExecuteNonQuery();
        }
        private static void LoadFile(NpgsqlConnection conexio)
        {
            // File path.
            string filePath = "../../../pokemon.csv";
            // Check if the CSV file exists.
            if (!File.Exists(filePath))
            {
                // Message stating CSV file could not be located.
                Console.WriteLine("Could not locate the CSV file.");
            }
            // Assign the CSV file to a reader object.
            StreamReader reader = new StreamReader(filePath);
            string line;
            string[] currentRow;
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Replace("['", "").Replace("']", "");
                Console.WriteLine(line);
                currentRow = line.Split(';');
                InsertDataPokemon(conexio,currentRow);
            }
        }
        private static void InsertDataPokemon(NpgsqlConnection con , string[] pokemonRow)
        {
            string sql = "INSERT INTO pokemon(abilities,against_bug,against_dark,against_dragon,against_electric,against_fairy,against_fight,against_fire,against_flying,against_ghost,against_grass,against_ground,against_ice,against_normal,against_poison,against_psychic,against_rock,against_steel,against_water,attack,base_egg_steps,base_happiness,base_total,capture_rate,classfication,defense,experience_growth,height_m,hp,name,percentage_male,pokedex_number,sp_attack,sp_defense,speed,type1,type2,weight_kg,generation,is_legendary) " +
                "VALUES(@abilities,@against_bug,@against_dark,@against_dragon,@against_electric,@against_fairy,@against_fight,@against_fire,@against_flying,@against_ghost,@against_grass,@against_ground,@against_ice,@against_normal,@against_poison,@against_psychic,@against_rock,@against_steel,@against_water,@attack,@base_egg_steps,@base_happiness,@base_total,@capture_rate,@classfication,@defense,@experience_growth,@height_m,@hp,@name,@percentage_male,@pokedex_number,@sp_attack,@sp_defense,@speed,@type1,@type2,@weight_kg,@generation,@is_legendary)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

            cmd.Parameters.AddWithValue("abilities", pokemonRow[0]);
            cmd.Parameters.AddWithValue("against_bug", pokemonRow[1]);
            cmd.Parameters.AddWithValue("against_dark", pokemonRow[2]);
            cmd.Parameters.AddWithValue("against_dragon", pokemonRow[3]);
            cmd.Parameters.AddWithValue("against_electric", pokemonRow[4]);
            cmd.Parameters.AddWithValue("against_fairy", pokemonRow[5]);
            cmd.Parameters.AddWithValue("against_fight", pokemonRow[6]);
            cmd.Parameters.AddWithValue("against_fire", pokemonRow[7]);
            cmd.Parameters.AddWithValue("against_flying", pokemonRow[8]);
            cmd.Parameters.AddWithValue("against_ghost", pokemonRow[9]);
            cmd.Parameters.AddWithValue("against_grass", pokemonRow[10]);
            cmd.Parameters.AddWithValue("against_ground", pokemonRow[11]);
            cmd.Parameters.AddWithValue("against_ice", pokemonRow[12]);
            cmd.Parameters.AddWithValue("against_normal", pokemonRow[13]);
            cmd.Parameters.AddWithValue("against_poison", pokemonRow[14]);
            cmd.Parameters.AddWithValue("against_psychic", pokemonRow[15]);
            cmd.Parameters.AddWithValue("against_rock", pokemonRow[16]);
            cmd.Parameters.AddWithValue("against_steel", pokemonRow[17]);
            cmd.Parameters.AddWithValue("against_water", pokemonRow[18]);
            cmd.Parameters.AddWithValue("attack", pokemonRow[19]);
            cmd.Parameters.AddWithValue("base_egg_steps", pokemonRow[20]);
            cmd.Parameters.AddWithValue("base_happiness", pokemonRow[21]);
            cmd.Parameters.AddWithValue("base_total", pokemonRow[22]);
            cmd.Parameters.AddWithValue("capture_rate", pokemonRow[23]);
            cmd.Parameters.AddWithValue("classfication", pokemonRow[24]);
            cmd.Parameters.AddWithValue("defense", pokemonRow[25]);
            cmd.Parameters.AddWithValue("experience_growth", pokemonRow[26]);
            cmd.Parameters.AddWithValue("height_m", pokemonRow[27]);
            cmd.Parameters.AddWithValue("hp", pokemonRow[28]);
            cmd.Parameters.AddWithValue("name", pokemonRow[29]);
            cmd.Parameters.AddWithValue("percentage_male", pokemonRow[30]);
            cmd.Parameters.AddWithValue("pokedex_number", pokemonRow[31]);
            cmd.Parameters.AddWithValue("sp_attack", pokemonRow[32]);
            cmd.Parameters.AddWithValue("sp_defense", pokemonRow[33]);
            cmd.Parameters.AddWithValue("speed", pokemonRow[34]);
            cmd.Parameters.AddWithValue("type1", pokemonRow[35]);
            cmd.Parameters.AddWithValue("type2", pokemonRow[36]);
            cmd.Parameters.AddWithValue("weight_kg", pokemonRow[37]);
            cmd.Parameters.AddWithValue("generation", pokemonRow[38]);
            cmd.Parameters.AddWithValue("is_legendary", pokemonRow[39]);
            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        //Ejercicio 7.1 Cargar datos CSV a BBDD
        public void CargarDatos(NpgsqlConnection conexio)
        {
            if (conexio.State == System.Data.ConnectionState.Open)
            {
                Console.WriteLine("Conectado");
                CreateTable(conexio);
                LoadFile(conexio);
            }
        }

        //Ejercicio 7.2 Crear instancia de tipo pokemon de la tabla de la BBDD
        public void CrearInstanciasPokemon(NpgsqlConnection conexio, List<Pokemon> pokedex)
        {
            string sql = "SELECT pokedex_number,name,generation,type1,type2,is_legendary FROM pokemon";
            using var cmd = new NpgsqlCommand(sql, conexio);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();

            //Hacemos las Instancias y los añadimos a la Lista
            while (rdr.Read())
            {

                pokedex.Add(new Pokemon(rdr.GetString(0),rdr.GetString(1),rdr.GetString(2),rdr.GetString(3),rdr.GetString(4),rdr.GetString(5) ));
            }
        }

        //Ejercicio 7.3 Hacer Update en la BBDD a Charmander
        public void UpdateCharmander(NpgsqlConnection conexio)
        {
            using var cmd = new NpgsqlCommand();
            cmd.Connection = conexio;

            cmd.CommandText = "update pokemon set type1 = @type1 where name = @name";
            cmd.Parameters.AddWithValue("type1", "Fuego");
            cmd.Parameters.AddWithValue("name", "Charmander");
            cmd.ExecuteNonQuery();
            Console.WriteLine("Type of charmander updated");
        }

        //Ejercicio 7.4 Eliminar los Legendarios de la tabla pokemon de la BBDD
        public void DeleteLegendaries(NpgsqlConnection conexio) {
            using var cmd = new NpgsqlCommand();
            cmd.Connection = conexio;

            cmd.CommandText = "delete from pokemon where is_legendary = @is_legendary";
            cmd.Parameters.AddWithValue("is_legendary", "1");
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            Console.WriteLine("Legendaries Deleted");

        }

        //Ejercicio 7.5 Crear una tabla de Pokemons Legendarios en la BBDD
        public void CreateTableLegendaries(NpgsqlConnection con)
        {
            string sql = "CREATE TABLE Legendarios (" +
                  "pokedex_number CHAR (250)  ," +
                  "name CHAR (250)," +
                  "generation CHAR (250)," +
                  "type1 CHAR (250)," +
                  "type2 CHAR (250))";

            NpgsqlCommand commmand = new NpgsqlCommand(sql, con);
            commmand.ExecuteNonQuery();
            LoadFile2(con);
        }
        private static void LoadFile2(NpgsqlConnection conexio)
        {
            // File path.
            string filePath = "../../../pokemon.csv";
            // Check if the CSV file exists.
            if (!File.Exists(filePath))
            {
                // Message stating CSV file could not be located.
                Console.WriteLine("Could not locate the CSV file.");
            }
            // Assign the CSV file to a reader object.
            StreamReader reader = new StreamReader(filePath);
            string line;
            string[] currentRow;
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Replace("['", "").Replace("']", "");
                Console.WriteLine(line);
                currentRow = line.Split(';');
                if (currentRow[39] == "1")
                {
                    InsertDataLegendary(conexio, currentRow);
                }
                
            }
        }
        private static void InsertDataLegendary(NpgsqlConnection con, string[] pokemonRow)
        {
            string sql = "INSERT INTO Legendarios(pokedex_number,name,generation,type1,type2)" +
                "VALUES(@pokedex_number,@name,@generation,@type1,@type2)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

            cmd.Parameters.AddWithValue("pokedex_number", pokemonRow[31]);
            cmd.Parameters.AddWithValue("name", pokemonRow[29]);
            cmd.Parameters.AddWithValue("generation", pokemonRow[38]);
            cmd.Parameters.AddWithValue("type1", pokemonRow[35]);
            cmd.Parameters.AddWithValue("type2", pokemonRow[36]);
            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        static void Main(string[] args)
        {
            //Conexion a la BD -> con
            var conexioString = "Host=localhost;Username=postgres;Password=;Database=m3bbdd";
            using var conexio = new NpgsqlConnection(conexioString);
            conexio.Open();
            PersistenciaDDBBAngelNavarrete app = new PersistenciaDDBBAngelNavarrete();
            app.CargarDatos(conexio);
            List<Pokemon> pokedex = new List<Pokemon>();
            app.CrearInstanciasPokemon(conexio, pokedex);
            //Mostramos las Instancias de los pokemons
            foreach (var pokemon in pokedex)
            {
                Console.WriteLine(pokemon.ToString());
            }
            app.UpdateCharmander(conexio);
            app.DeleteLegendaries(conexio);
            //app.CreateTableLegendaries(conexio);
            conexio.Close();
        }
    }
}
